﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramaCriptografiaAPS
{
    class Program
    {
        //Inicio para procedimento de Criptografar
        public static void Criptografar(string dados)
        {
            // pega o tamanho dos dados no formato string que o usuário digitou (numero de letras), e subtrai por 1 porque o índice vetor string começa do número 0
            int tamanho = dados.Length -1;
            string dadosCriptografados = null;

            // faz um looping com o tamanho dos dados que o usúario digitou
            for (int i = 0; i<=tamanho; i++)
            {
                dadosCriptografados += (TrocaParaCriptografia(dados[i].ToString()));
            }

            Console.WriteLine("A frase criptografada é : \n" + dadosCriptografados);
            Console.ReadKey();
        }
        public static string TrocaParaCriptografia(string dados)
        {
            // vetor com as strings de entrada digitada pelo usuário 
            string[] letras = { "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "Á", "À", "Â", "Ã", "á", "à", "â", "ã", "É", "È", "Ê", "é", "è", "ê", "Í", "Ì", "í", "ì", "Ó", "Ò", "Ô", "Õ", "ó", "ò", "ô", "õ", "Ú", "Ù", "Û", "ú", "ù", "û", "Ç", "ç", "+", "-", "*", "/", ".", "@", "#", "$", "%", "&", "(", ")", "_", ":", "<", ">", "=", "?", "{", "}", "|", "~", "^", "`", "'", ";", "[", "]", "!", ",", " " };

            // vetor com os códigos hexadecimais para fazer a cifragem
            string[] trocaLetras = { "33", "34", "35", "36", "37", "38", "39", "30", "31", "32", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4A", "4B", "4C", "4D", "4E", "4F", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5A", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6A", "6B", "6C", "6D", "6E", "6F", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7A", "B5", "B7", "B6", "C7", "A0", "85", "83", "C6", "90", "D4", "D2", "82", "8A", "88", "D6", "DE", "ED", "8D", "E0", "E3", "E2", "E5", "A2", "95", "93", "E4", "E9", "EB", "EA", "A3", "97", "96", "80", "87", "2B", "2D", "2A", "2F", "2E", "40", "23", "24", "25", "26", "28", "29", "5F", "3A", "3C", "3E", "3D", "3F", "7B", "7D", "7C", "7E", "5E", "60", "27", "3B", "5B", "5D", "21", "2C", "20" };

            // recebe os dados que o usuário digitou e armazena na variável dadosRecebidos
            string dadosRecebidos = dados;

            // faz a leitura dos dois vetores, e troca as strings do vetor letras, pelos valores strings (Hexadecimais) do vetor trocaLetras
            for (int i = 0; i < 127; i++)
            {
                string dadosOrigem = letras[i];
                string dadosTroca = trocaLetras[i];
                dadosRecebidos = dadosRecebidos.Replace(dadosOrigem, dadosTroca);
            }
            return dadosRecebidos;
        }
        //Fim procedimento Criptografar

        //Inicio procedimento Descriptografar
        public static void Descriptografar(string dados)
        {
            // pega o tamanho dos dados no formato string que o usuário digitou (numero de letras), e subtrai por 1 porque o índice vetor string começa do número 0
            int tamanhoDados = 0;
            tamanhoDados = dados.Length - 1;
            int i = 0;

            // variável que ira armazenar o valor descriptogrado
            string dadosDescriptogrados = null;

            // faz um looping com o tamanho dos dados que o usúario escreveu
            for (i = 0; i <= tamanhoDados; i++)
            {
                // faz um looping pegando os 2 número do vetor trocaLetras
                for (int ii = 0; ii <= 1; i++)
                {
                    dadosDescriptogrados += dados[i].ToString();
                    ii++;
                }

                // escreve letra por letra que está sendo descriptograda
                Console.Write(TrocaParaDesriptografia(dadosDescriptogrados));
                i--;
                dadosDescriptogrados = null;
            }
            Console.ReadKey();
        }

        public static string TrocaParaDesriptografia(string dados)
        {
            // vetor com as strings de saída para a tela do usuário
            string[] troca = { "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "Á", "À", "Â", "Ã", "á", "à", "â", "ã", "É", "È", "Ê", "é", "è", "ê", "Í", "Ì", "í", "ì", "Ó", "Ò", "Ô", "Õ", "ó", "ò", "ô", "õ", "Ú", "Ù", "Û", "ú", "ù", "û", "Ç", "ç", "+", "-", "*", "/", ".", "@", "#", "$", "%", "&", "(", ")", "_", ":", "<", ">", "=", "?", "{", "}", "|", "~", "^", "`", "'", ";", "[", "]", "!", ",", " " };

            // vetor com os códigos hexadecimais para fazer a decifragem 
            string[] origem = { "33", "34", "35", "36", "37", "38", "39", "30", "31", "32", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4A", "4B", "4C", "4D", "4E", "4F", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5A", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6A", "6B", "6C", "6D", "6E", "6F", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7A", "B5", "B7", "B6", "C7", "A0", "85", "83", "C6", "90", "D4", "D2", "82", "8A", "88", "D6", "DE", "ED", "8D", "E0", "E3", "E2", "E5", "A2", "95", "93", "E4", "E9", "EB", "EA", "A3", "97", "96", "80", "87", "2B", "2D", "2A", "2F", "2E", "40", "23", "24", "25", "26", "28", "29", "5F", "3A", "3C", "3E", "3D", "3F", "7B", "7D", "7C", "7E", "5E", "60", "27", "3B", "5B", "5D", "21", "2C", "20" };

            // recebe os dados que o usuário digitou e armazena dadosRecebidos
            string dadosRecebidos = dados;

            // faz a leitura dos dois vetores, e troca as strings do vetor origem, pelas strings do vetor troca
            for (int i = 0; i < 127; i++)
            {
                string dadosOrigem = origem[i];
                string dadosTroca = troca[i];
                dadosRecebidos = dadosRecebidos.Replace(dadosOrigem, dadosTroca);
            }
            return dadosRecebidos;
        }
        //Fim procedimento Descriptografar
        static void Main(string[] args)
        {
            //Espaço para variáveis
            string OpcaoSair, OpcaoUsuario;
            
            //Inicio do código
            //Inicio do Programa e/ou Recomeçar
            do
            {
                //Inicio comando para limpar tela
                Console.Clear();
                //Fim comando para limpar tela

                //Inicio Cabeçalho do programa
                Console.WriteLine("Este programa faz a Criptografia e/ou Descriptografia de textos escrito neste console!!!");
                //Fim Cabeçalho do programa

                //Inicio menu de escolha do usuário
                Console.Write("Por favor, digite opção C para Criptografar, D para descriptografar ou digite qualquer outra tecla para sair: ");
                OpcaoUsuario = Console.ReadLine();
                //Fim menu de escolha do usuário

                //Inicio escolha de opções do usuário
                switch (OpcaoUsuario)
                {
                    //Inicio caso opção escolhida seja  C ou c Criptografar
                    case "C":
                    case "c":
                        Console.WriteLine("Digite o que deseja criptografar");
                        string Nome = Console.ReadLine();
                        Console.WriteLine(Nome);
                        Criptografar(Nome);
                        break;
                    //Fim caso opção escolhida se  C ou c Criptografar

                    //Inicio caso opção escolhida seja D ou d Descriptografar
                    case "D":
                    case "d":
                        Console.WriteLine("Digite o que deseja descriptografar");
                        Nome = Console.ReadLine();
                        Descriptografar(Nome);
                        break;
                    //Fim caso opção escolhida seja D ou d Descriptografar
                }
                //Fim escolha opção do usuário

                //Inicio menu Sair do Programa ou Recomeçar
                Console.WriteLine("Deseja mesmo sair do programa? ");
                Console.Write("Digite S para sair ou qualquer outra tecla para ficar: ");
                OpcaoSair = Console.ReadLine();
                //Fim menu Sair do Programa ou Recomeçar
            } while ((OpcaoSair != "S") && (OpcaoSair != "s"));
        }
    }
} 
